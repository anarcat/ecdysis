=====================================================
 ecdysis - personal code and documentation templates
=====================================================

This project is a set of templates and code snippets I reuse by
copy-pasting in other projects. They are generally not worth creating
a full module or project for, since they are often small
one-liners. Or they are too complex and overlap with existing
functionalities. Ideally, those would be merged in the standard
library.

This here is chaos and may make sense only to me.

.. image:: https://gitlab.com/anarcat/ecdysis/badges/master/pipeline.svg
   :alt: pipeline status
   :target: https://gitlab.com/anarcat/ecdysis/commits/master

.. image:: https://gitlab.com/anarcat/ecdysis/badges/master/coverage.svg
   :alt: coverage report
   :target: https://gitlab.com/anarcat/ecdysis/commits/master

.. image:: https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg 
   :alt: Say thanks to the author
   :target: https://saythanks.io/to/anarcat

.. only relevant if package is in PyPI: it's not right now.
.. .. image:: https://img.shields.io/pypi/l/ecdysis.svg
..    :alt: MIT licensed
..    :target: https://gitlab.com/anarcat/ecdysis/blob/master/LICENSE
.. 
.. .. image:: https://img.shields.io/pypi/v/feed2exec.svg
..    :alt: feed2exec version on PyPI
..    :target: https://pypi.python.org/pypi/feed2exec
.. 
.. Only relevant if package is in Debian, it's not right now:
.. .. image:: https://badges.debian.net/badges/debian/stable/feed2exec/version.svg
..    :alt: feed2exec version in stable
..    :target: https://packages.debian.org/stable/feed2exec
.. 
.. .. image:: https://badges.debian.net/badges/debian/unstable/feed2exec/version.svg
..    :alt: feed2exec version in unstable
..   :target: https://packages.debian.org/unstable/feed2exec

.. marker-toc

Usage
=====

Quick start
-----------

Copy the code you need from the `ecdysis/` module. Search and replace
the ``ecdysis`` string with your own module name. Backport to Python 2
if you need: the code targets Python 3, but should be easily
backportable. Make sure you also check the ``setup.py``,
``setup.cfg``, ``tox.ini``, ``.gitignore`` and similar files from the
top directory to setup tests properly.

Copy the ``doc/`` directory into your project. Review and edit the
:doc:`contribute` guidelines and run ``make html`` to generate a HTML
rendering of the documentation. Read the :doc:`API documentation
<code>` and copy-paste the code you need. Consider adding a `changelog
<https://github.com/olivierlacan/keep-a-changelog>`_.

We use the `doctest <https://docs.python.org/3/library/doctest.html>`_
module to perform tests on the various functions because it allows us
to quickly copy the tests along with the functions when we copy code
around. Tests are discovered with `pytest <https://pytest.org/>`_.

Documentation structure
-----------------------

Code is not everything. Documentation is really important too. This
base package also features extensive self-documentation, but also
documentation templates that can be reused.

The documentation is made of this ``README`` file, but is also
rendered as a ReST (REStructured Text) document that is rendered into
various formats (HTML, ePUB, PDF, etc) through the `Sphinx`_
documentation system. Special includes in the ``index.rst`` file do
some magic to distribute parts of this file in the right sections of
the online documentation.

 .. _Sphinx: http://www.sphinx-doc.org

Community guidelines
--------------------

The community guidelines are described in the :doc:`contribute`
document, which provides a nice template that I reuse in other
projects. It includes:

 * a code of conduct
 * how to send patches
 * how documentation works
 * how to report bugs
 * how to make a release

It seems critical to me that every project should have such
documentation.

Why the name?
-------------

The name comes from what snakes and other animals do to "create a new
snake": they shed their skin. This is not so appropriate for snakes,
as it's just a way to rejuvenate their skin, but is especially
relevant for anthropods since the ecdysis may be associated with a
metamorphosis:

    Ecdysis is the moulting of the cuticle in many invertebrates of
    the clade Ecdysozoa. Since the cuticle of these animals typically
    forms a largely inelastic exoskeleton, it is shed during growth
    and a new, larger covering is formed. The remnants of the old,
    empty exoskeleton are called exuviae.

    -- `Wikipedia <https://en.wikipedia.org/wiki/Ecdysis>`_

So this project is *metamorphosed* into others when the documentation
templates, code examples and so on are reused elsewhere. For that
reason, the license is an unusally liberal (for me) MIT/Expat license.

The name also has the nice property of being absolutely
unpronounceable, which makes it unlikely to be copied but easy to
search online.
