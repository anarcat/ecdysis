# -*- coding: utf-8 -*-

"""main ecdysis command

this is in __main__.py so that we can call this with:

    python -m ecdysis

it also allows us to load package metadata from __init__.py without
requiring all dependencies to be installed at build time

A simpler example of a basic script is in my templates here:

https://gitlab.com/anarcat/emacs-d/blob/master/snippets/python-mode/script
"""
# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

import click

import ecdysis


@click.command(
    help=ecdysis.__description__,
    context_settings=dict(help_option_names=["-h", "--help"]),
)
@click.version_option(version=ecdysis.version)
@click.option(
    "--loglevel",
    "loglevel",
    help="show only warning messages",
    flag_value="WARNING",
    default=True,
)
@click.option("-v", "--verbose", "loglevel", help="be more verbose", flag_value="INFO")
@click.option("-d", "--debug", "loglevel", help="even more verbose", flag_value="DEBUG")
def main(loglevel):
    logging.basicConfig(level=loglevel.upper())
    try:
        logging.info("hello world")
    except Exception as e:
        logging.error("unexpected error: %s", e)
        if loglevel == "DEBUG":
            logging.warning(
                'starting debugger, type "c" and enter to continue'
            )  # noqa: E501
            import pdb
            import sys
            import traceback

            traceback.print_exc()
            pdb.post_mortem()
            sys.exit(1)
        raise e


if __name__ == "__main__":
    main()
