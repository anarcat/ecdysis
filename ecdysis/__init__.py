# -*- coding: utf-8 -*-

"""this file's purpose is mostly to store metadata about the
project. actual functionality should be in sub-modules or in
``__main__.py``."""

from __future__ import absolute_import, print_function

try:
    from ._version import version
except ImportError:
    try:
        from setuptools_scm import get_version

        version = get_version()
    except (ImportError, LookupError):
        version = "???"

__version__ = version
__description__ = """Project template. Community guidelines and git instructions and so
on. Code I often reuse. Mostly python-specific, but some parts
(e.g. documentation) may be useful for other projects."""
__website__ = "https://gitlab.com/anarcat/ecdysis"
__prog__ = "ecdysis"
__author__ = "Antoine Beaupré"
__email__ = "anarcat@debian.org"
__copyright__ = "Copyright (C) 2017 Antoine Beaupré"
__license_short__ = "AGPLv3"
__license__ = """
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
