"""time-related utilities"""

from __future__ import absolute_import

import datetime
import os
import time

import humanize
import psutil


class Timer(object):
    """this class is to track time and resources passed

    originally from bup-cron, but improved to include memory usage.

    More elaborate version, with a context manager, decorator, logger,
    formatter, named timers, etc.

    https://pypi.org/project/codetiming/
    """

    def __init__(self):
        """initialize the timstamp"""
        # we track start time and the timestamp separately because
        # datetime can go backwards. this means a slight inaccuracy
        # between "now - start" and the "diff()", but that's something
        # we should be able to tolerate, as the start time wasn't
        # shown in the public interface (except from self.stamp, which
        # didn't explicitly say it was a datetime)
        self.start = datetime.datetime.now()
        self._stamp = time.monotonic_ns()

    def times(self):
        """return a string designing resource usage"""
        return "user %s system %s chlduser %s chldsystem %s" % os.times()[:4]

    def rss(self):
        process = psutil.Process(os.getpid())
        return process.memory_info().rss

    def memory(self):
        return "RSS %s" % humanize.naturalsize(self.rss())

    def diff(self):
        """a datediff between the creation of the object and now"""
        return (time.monotonic_ns() - self._stamp) / 1000**3

    def __str__(self):
        """return a string representing the time passed and resources used"""
        return "elapsed: %s, started: %s (%s %s)" % (
            self.diff(),
            self.start,
            self.times(),
            self.memory(),
        )
