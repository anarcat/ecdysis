# -*- coding: utf-8 -*-

"""various string-related utilities"""

from __future__ import absolute_import

import datetime
import re
import time

from unidecode import unidecode


def slug(text):
    """Make a URL-safe, human-readable version of the given text

    This will do the following:

    1. decode unicode characters into ASCII
    2. shift everything to lowercase
    3. strip whitespace
    4. replace other non-word characters with dashes
    5. strip extra dashes

    This somewhat duplicates the :func:`Google.slugify` function but
    slugify is not as generic as this one, which can be reused
    elsewhere.

    >>> slug('test')
    'test'
    >>> slug('Mørdag')
    'mordag'
    >>> slug("l'été c'est fait pour jouer")
    'l-ete-c-est-fait-pour-jouer'
    >>> slug(u"\xe7afe au lait (boisson)")
    'cafe-au-lait-boisson'
    >>> slug(u"Multiple  spaces -- and symbols! -- merged")
    'multiple-spaces-and-symbols-merged'

    This is a simpler, one-liner version of the `slugify module
    <https://github.com/un33k/python-slugify>`_.
    """
    return re.sub(r"\W+", "-", unidecode(text).lower().strip()).strip("-")


def sizeof_fmt(
    num, suffix="B", units=None, power=None, sep=" ", precision=2, sign=False
):
    """format the given size as a human-readable size"""
    prefix = "+" if sign and num > 0 else ""

    for unit in units[:-1]:
        if abs(round(num, precision)) < power:
            if isinstance(num, int):
                return "{}{}{}{}{}".format(prefix, num, sep, unit, suffix)
            else:
                return "{}{:3.{}f}{}{}{}".format(
                    prefix, num, precision, sep, unit, suffix
                )
        num /= float(power)
    return "{}{:.{}f}{}{}{}".format(prefix, num, precision, sep, units[-1], suffix)


def sizeof_fmt_iec(num, suffix="B", sep=" ", precision=2, sign=False):
    return sizeof_fmt(
        num,
        suffix=suffix,
        power=1024,
        units=["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi", "Yi"],
        sep=sep,
        precision=precision,
        sign=sign,
    )


def sizeof_fmt_decimal(num, suffix="B", sep=" ", precision=2, sign=False):
    """
    # no rounding necessary for those
    >>> sizeof_fmt_decimal(0)
    '0 B'
    >>> sizeof_fmt_decimal(1)
    '1 B'
    >>> sizeof_fmt_decimal(142)
    '142 B'
    >>> sizeof_fmt_decimal(999)
    '999 B'
    >>> # rounding starts here
    >>> sizeof_fmt_decimal(1000)
    '1.00 kB'
    >>> # should be rounded away
    >>> sizeof_fmt_decimal(1001)
    '1.00 kB'
    >>> # should be rounded down
    >>> sizeof_fmt_decimal(1234)
    '1.23 kB'
    >>> # should be rounded up
    >>> sizeof_fmt_decimal(1235)
    '1.24 kB'
    >>> # rounded down as well
    >>> sizeof_fmt_decimal(1010)
    '1.01 kB'
    >>> # rounded down
    >>> sizeof_fmt_decimal(999990000)
    '999.99 MB'
    >>> # rounded down
    >>> sizeof_fmt_decimal(999990001)
    '999.99 MB'
    >>> # rounded up to next unit
    >>> sizeof_fmt_decimal(999995000)
    '1.00 GB'
    >>> # and all the remaining units, megabytes
    >>> sizeof_fmt_decimal(10**6)
    '1.00 MB'
    >>> # gigabytes
    >>> sizeof_fmt_decimal(10**9)
    '1.00 GB'
    >>> # terabytes
    >>> sizeof_fmt_decimal(10**12)
    '1.00 TB'
    >>> # petabytes
    >>> sizeof_fmt_decimal(10**15)
    '1.00 PB'
    >>> # exabytes
    >>> sizeof_fmt_decimal(10**18)
    '1.00 EB'
    >>> # zottabytes
    >>> sizeof_fmt_decimal(10**21)
    '1.00 ZB'
    >>> # yottabytes
    >>> sizeof_fmt_decimal(10**24)
    '1.00 YB'
    >>> # negative value
    >>> sizeof_fmt_decimal(-1)
    '-1 B'
    >>> # negative value with rounding
    >>> sizeof_fmt_decimal(-1010)
    '-1.01 kB'
    """
    return sizeof_fmt(
        num,
        suffix=suffix,
        power=1000,
        units=["", "k", "M", "G", "T", "P", "E", "Z", "Y"],
        sep=sep,
        precision=precision,
        sign=sign,
    )


def safe_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime.datetime, datetime.date, datetime.time)):
        return obj.isoformat()
    elif isinstance(obj, time.struct_time):
        return time.strftime("%c")
    else:
        return str(obj)
