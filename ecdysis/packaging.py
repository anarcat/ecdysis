from __future__ import absolute_import

import inspect
import os.path

import pkg_resources


def find_parent_module():
    """find the name of a the first module calling this module

    if we cannot find it, we return the current module's name
    (__name__) instead.
    """
    try:
        frame = inspect.currentframe().f_back
        module = inspect.getmodule(frame)
        while module is None or module.__name__ == __name__:
            frame = frame.f_back
            module = inspect.getmodule(frame)
        return module.__name__
    except AttributeError:
        # somehow we failed to find our module
        # return the logger module name by default
        return __name__


def find_static_file(path, module=None):
    """locate a file in the distribution

    this will look in the shipped files in the package

    this assumes the files are at the root of the package or the
    source tree (if not packaged)

    this does not check if the file actually exists.

    :param str path: path for the file, relative to the source tree root
    :param str module: name of the module to find the find in. if
                       None, guessed with :func:`find_parent_module`
    :return: the absolute path to the file
    """
    if module is None:
        module = find_parent_module()
    try:
        pkg = pkg_resources.Requirement.parse(module)
        filename = pkg_resources.resource_filename(pkg, os.path.join(module, path))
    except pkg_resources.DistributionNotFound:
        filename = os.path.join(os.path.dirname(__file__), path)
    return os.path.realpath(filename)
