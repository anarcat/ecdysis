"""various overrides to the builtin os library"""

from __future__ import absolute_import

import errno
import os
import os.path


def make_dirs_helper(path):
    """Create the directory if it does not exist

    Return True if the directory was created, false if it was already
    present, throw an OSError exception if it cannot be created

    >>> import tempfile
    >>> import os
    >>> import os.path as p
    >>> d = tempfile.mkdtemp()
    >>> make_dirs_helper(p.join(d, 'foo'))
    True
    >>> make_dirs_helper(p.join(d, 'foo'))
    False
    >>> make_dirs_helper(p.join('/dev/null', 'foo')) # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    NotADirectoryError: [Errno 20] Not a directory: ...
    >>> os.rmdir(p.join(d, 'foo'))
    >>> os.rmdir(d)
    >>>
    """
    try:
        os.makedirs(path)
        return True
    except OSError as ex:
        if ex.errno != errno.EEXIST or not os.path.isdir(path):
            raise
        return False
