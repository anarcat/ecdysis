Code of conduct
===============

.. note::

   Before you reuse this document in your own project, you will need
   to at least read the whole thing and make a few changes. Read more
   about community guidelines, code of conducts and
   `intersectionality`_ (also on `geek feminism wiki`_) before
   adopting this: it is not just a rubber stamp, a badge to add to
   your project, but a real commitment with complex ethical
   ramification. Concretely, you will at least need to make sure there
   is a :doc:`contact` section that details who can handle complaints
   and remove or comment out this note.

 .. _intersectionality: https://en.wikipedia.org/wiki/Intersectionality
 .. _geek feminism wiki: http://geekfeminism.wikia.com/wiki/Intersectionality

Contributor Covenant Code of Conduct
------------------------------------

Our Pledge
~~~~~~~~~~

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our
project and our community a harassment-free experience for everyone,
regardless of age, body size, disability, ethnicity, gender identity and
expression, level of experience, nationality, personal appearance, race,
religion, or sexual identity and orientation.

Our Standards
~~~~~~~~~~~~~

Examples of behavior that contributes to creating a positive environment
include:

-  Using welcoming and inclusive language
-  Being respectful of differing viewpoints and experiences
-  Gracefully accepting constructive criticism
-  Focusing on what is best for the community
-  Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

-  The use of sexualized language or imagery and unwelcome sexual
   attention or advances
-  Trolling, insulting/derogatory comments, and personal or political
   attacks
-  Public or private harassment
-  Publishing others' private information, such as a physical or
   electronic address, without explicit permission
-  Other conduct which could reasonably be considered inappropriate in a
   professional setting

Our Responsibilities
~~~~~~~~~~~~~~~~~~~~

Project maintainers are responsible for clarifying the standards of
acceptable behavior and are expected to take appropriate and fair
corrective action in response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit,
or reject comments, commits, code, wiki edits, issues, and other
contributions that are not aligned to this Code of Conduct, or to ban
temporarily or permanently any contributor for other behaviors that they
deem inappropriate, threatening, offensive, or harmful.

Scope
~~~~~

This Code of Conduct applies both within project spaces and in public
spaces when an individual is representing the project or its community.
Examples of representing a project or community include using an
official project e-mail address, posting via an official social media
account, or acting as an appointed representative at an online or
offline event. Representation of a project may be further defined and
clarified by project maintainers.

Enforcement
~~~~~~~~~~~

Instances of abusive, harassing, or otherwise unacceptable behavior may
be reported by contacting one of the persons listed in :doc:`contact`. All
complaints will be reviewed and investigated and will result in a
response that is deemed necessary and appropriate to the circumstances.
The project maintainers is obligated to maintain confidentiality with
regard to the reporter of an incident. Further details of specific
enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in
good faith may face temporary or permanent repercussions as determined
by other members of the project's leadership.

Project maintainers are encouraged to follow the spirit of the `Django
Code of Conduct Enforcement Manual`_ when receiving reports.

.. _Django Code of Conduct Enforcement Manual: https://www.djangoproject.com/conduct/enforcement-manual/

Attribution
~~~~~~~~~~~

This Code of Conduct is adapted from the `Contributor Covenant`_,
version 1.4, available at http://contributor-covenant.org/version/1/4.

 .. _Contributor Covenant: http://contributor-covenant.org

Changes
~~~~~~~

The Code of Conduct was modified to refer to *project maintainers*
instead of *project team* and small paragraph was added to refer to the
Django enforcement manual.

    Note: We have so far determined that writing an explicit enforcement
    policy is not necessary, considering the available literature
    already available online and the relatively small size of the
    community. This may change in the future if the community grows
    larger.
