Contribution guide
==================

This document outlines how to contribute to this project. It details
instructions on how to submit issues, bug reports and patches.

Before you participate in the community, you should agree to respect
the :doc:`code`.

.. include:: refs.rst.inc

.. note::

   Before you reuse this document in your own project, you will need
   to at least read the whole thing and make a few
   changes. Concretely, you will at least need to do the following
   changes:

          * change the references at the top of the file to point to
            your project

          * change the release process to follow your workflow (or
            remove it if releases are against your religion, which
            would be sad)

          * also consider using a tool like `DCO`_ to assign copyright
            ownership

          * obviously, remove or comment out this note when done

 .. _DCO: https://developercertificate.org/

Positive feedback
-----------------

Even if you have no changes, suggestions, documentation or bug reports
to submit, even just positive feedback like "it works" goes a long
way. It shows the project is being used and gives instant
gratification to contributors. So we welcome emails that tell us of
your positive experiences with the project or just thank you
notes. Head out to :ref:`contact` for contact informations or submit
a closed issue with your story.

You can also send your "thanks" through `saythanks.io
<https://saythanks.io/to/anarcat>`_.

.. image:: https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg 
   :alt: Say thanks to the author
   :target: https://saythanks.io/to/anarcat

Documentation
-------------

We love documentation!

The documentation resides in various `Sphinx
<http://www.sphinx-doc.org/>`_ documentations and in the README
file. Those can can be `edited online`_ once you register and changes
are welcome through the normal patch and merge request system.

Issues found in the documentation are also welcome, see below to file
issues in our tracker.

Issues and bug reports
----------------------

We want you to report issues you find in the software. It is a
recognized and important part of contributing to this project. All
issues will be read and replied to politely and
professionnally. Issues and bug reports should be filed on the `issue
tracker <issues_>`_.

Issue triage
~~~~~~~~~~~~

Issue triage is a useful contribution as well. You can review the
`issues`_ in the `project page <project_>`_ and, for each issue:

-  try to reproduce the issue, if it is not reproducible, label it with
   ``more-info`` and explain the steps taken to reproduce
-  if information is missing, label it with ``more-info`` and request
   specific information
-  if the feature request is not within the scope of the project or
   should be refused for other reasons, use the ``wontfix`` label and
   close the issue
-  mark feature requests with the ``enhancement`` label, bugs with
   ``bug``, duplicates with ``duplicate`` and so on...

Note that some of those operations are available only to project
maintainers, see below for the different statuses.

Security issues
~~~~~~~~~~~~~~~

Security issues should first be disclosed privately to the project
maintainers (see :doc:`contact`), which support receiving encrypted
emails through the usual OpenPGP key discovery mechanisms.

This project cannot currently afford bounties for security issues. We
would still ask that you coordinate disclosure, giving the project a
reasonable delay to produce a fix and prepare a release before public
disclosure.

Public recognition will be given to reporters security issues if
desired. We otherwise agree with the `Disclosure Guidelines`_ of the
`HackerOne project`_, at the time of writing.

.. _Disclosure Guidelines: https://www.hackerone.com/disclosure-guidelines
.. _HackerOne project: https://www.hackerone.com/

Patches
-------

Patches can be submitted through `merge requests`_ on the `project
page <project_>`_.

Some guidelines for patches:

-  A patch should be a minimal and accurate answer to exactly one
   identified and agreed problem.
-  A patch must compile cleanly and pass project self-tests on all
   target platforms.
-  A patch commit message must consist of a single short (less than 50
   characters) line stating a summary of the change, followed by a blank
   line and then a description of the problem being solved and its
   solution, or a reason for the change. Write more information, not
   less, in the commit log.
-  Patches should be reviewed by at least one maintainer before being
   merged.

Project maintainers should merge their own patches only when they have
been approved by other maintainers, unless there is no response within a
reasonable timeframe (roughly one week) or there is an urgent change to
be done (e.g. security or data loss issue).

As an exception to this rule, this specific document cannot be changed
without the consensus of all administrators of the project.

    Note: Those guidelines were inspired by the `Collective Code
    Construct Contract`_. The document was found to be a little too
    complex and hard to read and wasn't adopted in its entirety. See
    this `discussion`_
    for more information.

.. _Collective Code Construct Contract: https://rfc.zeromq.org/spec:42/C4/
.. _discussion: https://github.com/zeromq/rfc/issues?utf8=%E2%9C%93&q=author%3Aanarcat%20

Patch triage
~~~~~~~~~~~~

You can also review existing pull requests, by cloning the contributor's
repository and testing it. If the tests do not pass (either locally or
in the online Continuous Integration (CI) system), if the patch is
incomplete or otherwise does not respect the above guidelines, submit a
review with "changes requested" with reasoning.

Membership
----------

There are three levels of membership in the project, Administrator (also
known as "Owner" in GitHub or GitLab), Maintainer (also known as
"Member" on GitHub or "Developer" on GitLab), or regular users (everyone
with or without an account). Anyone is welcome to contribute to the
project within the guidelines outlined in this document, regardless of
their status, and that includes regular users.

Maintainers can:

-  do everything regular users can
-  review, push and merge pull requests
-  edit and close issues

Administrators can:

-  do everything maintainers can
-  add new maintainers
-  promote maintainers to administrators

Regular users can be promoted to maintainers if they contribute to the
project, either by participating in issues, documentation or pull
requests.

Maintainers can be promoted to administrators when they have given
significant contributions for a sustained timeframe, by consensus of the
current administrators. This process should be open and decided as any
other issue.

Release process
---------------

 .. _Semantic Versioning: http://semver.org/

.. note:: This is just an example. There is no official release
          process for the ecdysis project right now, as the module is
          not publicly released or versioned. This, also, could be
          significantly improved. In particular, it has a DRI problem
          with the way we have release notes in a CHANGELOG *and* in
          the git tag, `mpalmer argues this is wrong
          <https://www.hezmatt.org/~mpalmer/blog/2024/11/23/your-release-process-sucks.html>`_
          and argues that `change logs should be burned to the ground
          <https://www.hezmatt.org/~mpalmer/blog/2024/11/23/invalid-excuses-why-your-release-process-sucks.html>`_,
          although that doesn't resolve the issue with Debian
          packages. For that, `dgit
          <https://salsa.debian.org/dgit-team/dgit>`_ might be an
          answer, although `the introduction
          <https://manpages.debian.org/bookworm/dgit/dgit-user.7.en.html>`_
          still seems to rely on ``gbp dch`` which means you still
          need to make a changelog entry.
          He also wrote a neat `git version-bump command
          <https://github.com/mpalmer/git-version-bump>`_ that handles
          the ``git tag`` step below.

To make a release:

1. generate release notes with::

       gbp dch

   the file header will need to be moved back up to the beginning of
   the file. also make sure to add a summary and choose a proper
   version according to `Semantic Versioning`_

2. tag the release according to `Semantic Versioning`_ rules:

   ::

       git tag -s x.y.z

3. build and test the Python package:

   ::

       python setup.py bdist_wheel &&
       python3 -m venv ~/.venvs/ecdsysis --system-site-packages &&
       ~/.venvs/ecdsysis/bin/pip3 install $(ls -1tr dist/*.whl | tail -1) &&
       ~/.venvs/ecdsysis/bin/ecdsysis --version &&
       rm -rf ~/.venvs/feed2exec

4. build and test the debian package:

   ::

       git-buildpackage &&
       sudo dpkg -i $(ls -tr1 ../build-area/ecdysis_*.deb | tail -1) &&
       ecdysis --version &&
       sudo dpkg --purge ecdysis

5. push changes:

   ::

       git push
       git push --tags
       twine upload dist/*
       dput ../ecdysis*.changes

6. edit the `tag`_, copy-paste the changelog entry and attach the
   signed binaries
