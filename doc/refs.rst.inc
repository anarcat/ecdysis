.. Those need to change according to your own project.
..
.. make sure you review each line and also fix the link in webirc.html
..
.. |project_name| replace:: ecdysis
.. |irc_channel| replace:: ``#anarcat``
.. |webirc| replace:: https://webchat.freenode.net/?nick=ecdysis.&channels=ecdysis&prompt=1
.. _irc: ircs://irc.freenode.net/ecdysis/
.. _webirc: https://webchat.freenode.net/?nick=ecdysis.&channels=ecdysis&prompt=1
.. _project: https://gitlab.com/anarcat/ecdysis/
.. _merge requests: https://gitlab.com/anarcat/ecdysis/merge_requests
.. _tag: https://gitlab.com/anarcat/ecdysis/tags
.. _issues: https://gitlab.com/anarcat/ecdysis/issues
.. _edited online: https://gitlab.com/anarcat/ecdysis/edit/master/README.rst
