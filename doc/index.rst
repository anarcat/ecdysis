.. debmans documentation master file, created by
   sphinx-quickstart on Fri Nov 18 16:37:08 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst
   :end-before: .. marker-toc

Contents:

.. contents::
   :local:

.. toctree::
   :maxdepth: 2

   usage
   snippets
   support
   contribute
   code
   todo
   license
   contact
