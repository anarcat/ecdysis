Code snippets
=============

This is Python code snippets I often reuse between different
software. One day, maybe parts of this could be merged in the standard
library or at least shipped in a reusable library?

Code documentation
------------------

This is the automatically generated documentation for the Python code.

ecdysis.argparse
~~~~~~~~~~~~~~~~

.. automodule:: ecdysis.argparse
   :members:
   :undoc-members:

ecdysis.cli
~~~~~~~~~~~

.. automodule:: ecdysis.cli
   :members:
   :undoc-members:

ecdysis.logging
~~~~~~~~~~~~~~~

.. automodule:: ecdysis.logging
   :members:
   :undoc-members:

ecdysis.os
~~~~~~~~~~

.. automodule:: ecdysis.os
   :members:
   :undoc-members:

ecdysis.packaging
~~~~~~~~~~~~~~~~~

.. automodule:: ecdysis.packaging
   :members:
   :undoc-members:

ecdysis.strings
~~~~~~~~~~~~~~~

.. automodule:: ecdysis.strings
   :members:
   :undoc-members:

ecdysis.time
~~~~~~~~~~~~

.. automodule:: ecdysis.time
   :members:
   :undoc-members:
   :special-members:


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
