Contact
=======

This program was written by `Antoine Beaupré`_. Please do not send
email to maintainers privately unless you are looking for paid
consulting or support. See :doc:`contribute` for more information
about how to collaborate on this project.

As a special exception, security issues can be reported privately
using `this contact information <https://anarc.at/contact/>`_, where
OpenPGP key material is also available.

The following people have volunteered to be available to respond to Code
of Conduct reports. They have reviewed existing literature and agree to
follow the aforementioned process in good faith. They also accept
OpenPGP-encrypted email:

-  `Antoine Beaupré`_

.. _Antoine Beaupré: https://anarc.at/


.. todo:: https://github.com/kentcdodds/all-contributors?
