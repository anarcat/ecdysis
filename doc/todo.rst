Todo
====

README improvements
-------------------

For now, I have only included here the :doc:`contribute` document, but
more should be added. There are a lot of templates around for
``README`` files which we could inspire from:

 * `the art of README <https://github.com/noffle/art-of-readme>`_ -
   has a nice checklist, from the `common-readme
   <https://github.com/noffle/common-readme>`_ node.js module guy
 * NPM has another `standard readme spec
   <https://www.npmjs.com/package/standard-readme-spec>`_
 * Perl has `standards
   <http://perldoc.perl.org/perlmodstyle.html#DOCUMENTING-YOUR-MODULE>`_,
   which are basically derived from manpages
 * manpages, themselves, have a standard set of headings, well
   described in the `mdoc(7) manpage
   <https://manpages.debian.org/mdoc>`_
 * Drupal has its own set of `guidelines for README files
   <https://www.drupal.org/docs/develop/documenting-your-project/readme-template>`_
 * yet another "kickass" `README template <https://dev.to/scottydocs/how-to-write-a-kickass-readme-5af9>`_
 * Ben Ford has an article named `Writing a great README
   <https://binford2k.com/2020/04/21/great-readme/>`_ which recommends
   the 5/30/90 rule: 5 seconds to decide if the README is worth a
   read, 30 seconds for the main pitch, 90 seconds for architecture,
   performance and more in-depth questions, then "the rest" (links to
   the rest of the docs, how to install, etc)
 * finally, this list wouldn't be complete without a `good discussion
   on stackoverflow
   <https://stackoverflow.com/questions/2304863/how-to-write-a-good-readme>`_

I did a quick review of the Art of README checklist, and we're not too
bad for a template. Ironically, I should review my own writing in the
`bug reporting blog post <https://anarc.at/blog/2016-10-14-bug-reporting/>`_.

I wonder if I should adopt the `Semantic Line Breaks standard
<https://sembr.org/>`_.

Changelog and index
-------------------

The ``index.rst`` file should link to a template design document as
well, along with an example of how to build a manpage. A changelog may
be a good addition as well.

Man pages
---------

There's also this whole thing about syncing the inline ``--help`` with
documentation and the manpage. There's `help2man
<https://www.gnu.org/software/help2man/>`_ that can be useful for
simpler programs, and I have used it to bootstrap the manpage for
undertime (I think). Otherwise, there's a whole slew of half-broken
stuff to turn :mod:`argparse` output directly into a manpage in the
build system. This is how Monkeysign works. Finally, what I actually
prefer (and I do in stressant) is to write the manpage by hand, in
RST, and convert it to a manpage at build time.

See also my other projects (e.g. `monkeysign`_, `linkchecker`_,
`wallabako`_, `stressant`_, `debmans`_) for more examples of the
documentation layout.

 .. _monkeysign: https://monkeysphere.info/monkeysign
 .. _linkchecker: https://github.com/linkcheck/linkchecker
 .. _wallabako: https://gitlab.com/anarcat/wallabako/
 .. _stressant: https://gitlab.com/anarcat/stressant
 .. _debmans: http://debmans.readthedocs.io/

Commit messages
---------------

The contribution guidelines could benefit from improvements regarding
commit messages. People often write fairly bad commit messages in
patches and commits on projects I participate in. It's also the case
with bug reports, but we have fairly good instructions in the
:doc:`support` template here. Patches are specifically painful as
there are no templates that can force users to do the right
thing. There are some notes in the :doc:`contribute` document, but
they could be expanded. Some documents I need to review:

 * `How to Write a Git Commit Message
   <https://chris.beams.io/posts/git-commit/>`_ - establishes "seven
   rules":
   
   1. Separate subject from body with a blank line
   2. Limit the subject line to 50 characters
   3. Capitalize the subject line
   4. Do not end the subject line with a period
   5. Use the imperative mood in the subject line
   6. Wrap the body at 72 characters
   7. Use the body to explain what and why vs. how

 * `Linux kernel's SubmittingPatches documentation
   <https://github.com/torvalds/linux/blob/master/Documentation/process/submitting-patches.rst#2-describe-your-changes>`_:

   * one patch should fix only one thing
   * describe the problem
   * describe the impact
   * quantify optimizations and tradeoffs (benchmarks!)
   * describe what is being done
   * use the imperative form (e.g. "make foo" instead of "this makes foo")
   * refer to bug reports, URL, emails if relevant
   * refer to a commit hash if the commit fixes a regression
   * if you refer to a commit, mention the SHA-1 and the short
     description

   See also the `Git project SubmittingPatches document
   <https://github.com/git/git/blob/master/Documentation/SubmittingPatches>`_,
   which is similar or the `DISCUSSION section of the git-commit(1)
   manual
   <https://manpages.debian.org/bookworm/git-man/git-commit.1.en.html#DISCUSSION>`_.

 * `ProGit commit guidelines
   <https://www.git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project#_commit_guidelines>`_ -
   formatting tips and some of the above

 * `Romulo Oliveira's guide
   <https://github.com/RomuloOliveira/commit-messages-guide>`_ is also
   interesting

 * `A great commit message
   <https://fatbusinessman.com/2019/my-favourite-git-commit>`_ and
   `many more in this Hacker News discussion <https://news.ycombinator.com/item?id=21289827>`_

 * `Conventional commits <https://conventionalcommits.org/>`_ - a
   stricter approach?

 * `Patterns for writing better git commit messages
   <https://dev.to/helderburato/patterns-for-writing-better-git-commit-messages-4ba0>`_ -
   has good directives too

 * `Git Best Practices – How to Write Meaningful Commits, Effective
   Pull Requests, and Code Reviews
   <https://www.freecodecamp.org/news/git-best-practices-commits-and-code-reviews/>`_ -
   "imperative", "brief", "helpful", examples of bad commits, small,
   meaningful commits

Funding
-------

Another thing I'm looking at is donations for financing software
projects. I don't like donations too much because that is
charity-based, which skews social dynamics away from the republic and
towards capital, but that's another political discussion that cannot
be resolved in the short term. We still need to find ways of feeding
developers and the options are rare. Here are a few reviews worth
mentioning:

 * `review of funding approaches from Tyil
   <https://tyil.nl/articles/funding-yourself-as-free-software-developer.html>`_
 * `GitHub's Open Source funding guide
   <https://opensource.guide/getting-paid/>`_
 * Snowdrift has an excellent `market research
   <https://wiki.snowdrift.coop/market-research>`_ about all this,
   including payment gateways and crowdfunding platforms
 * `Other ideas from a random GitHub project
   <https://github.com/nayafia/lemonade-stand>`_
 * `License zero
   <https://writing.kemitchell.com/2017/09/12/The-License-Zero-Manifesto.html>`_
 * `Liberal Software <https://lwn.net/Articles/712376/>`_ and
   Percival's `paid support
   <http://www.daemonology.net/blog/2017-05-11-plan-for-foss-maintainers.html>`_
 * `GNOME apps funding <https://www.joaquimrocha.com/2017/09/05/paying-for-foss-apps/>`_

Whether any of this will be implemented in my projects remains an open
question, for which I am still looking `for feedback
<https://anarc.at/blog/2017-03-30-free-software-activities-march-2017/#looking-into-self-financing>`_. One
of the concerns is that launching a funding campaign that eventually
fails could have a net negative reputation and psychological
impacts. Furthermore, we may want to avoid supporting certain
platforms that `ban political speech
<https://www.dailydot.com/layer8/patreon-antifa-altright-igd-lauren-southern/>`_... This
is a minefield.

Code
----

I still don't know what to do with that code. Let loose, this could
become like Stackoverflow: a huge dump of random code. Ideally, the
following steps should be taken:

 1. 100% documentation coverage
 2. 100% test coverage
 3. parts or everything published as (a?) module(s?)
 4. parts or everything merged in the standard library
 5. type-checking (`mypy`_, `pyright`_, `feed2exec`_ uses the former)

.. _mypy: https://mypy.readthedocs.io/
.. _pyright: https://github.com/Microsoft/pyright
.. _feed2exec: https://feed2exec.readthedocs.io/

Stuff like the logging handlers, in particular, should especially be
considered for merging. On the other hand, I also like the idea of
simply copy-pasting small bits of code as needed. There *is* already a
`slugify <https://github.com/un33k/python-slugify>`_ module - yet my
:func:`ecdysis.slug` function is still useful because it's much
simpler and it's a one-liner that can be copy-pasted in your code
without adding another dependency...

Note that code is nevertheless split up in modules that match the
upstream module names where they could possibly end up, when relevant.

There are other code snippets that are not included here yet, because
I'm not sure they're good enough or that I would actually reuse them:

 * for pid files, I wrote my own PidFile class in bup-cron, but should
   look at lockfile

 * to run commands, stressant has this useful `collectCmd` function to
   collect output of running commands. bup-cron also has shit like
   that.

 * for ``setup.py``, monkeysign has things to generate manpages (I
   used Sphinx instead in stressant), automatically call sphinx from
   the build chain, and translation stuff. debmans also has a neat
   __main__ hook. openstack's pbr project may be relevant here as
   well.

 * monkeysign also has a UI abstraction layer that well... works more
   or less well, but at least works.

 * gameclock also has some neat ideas that may be reused

Finally, it looks like Python is moving away from ``setup.py`` to
build packages. Some tools have started using `pyproject.toml
<https://snarky.ca/what-the-heck-is-pyproject-toml/>`_ instead, like
`flit <https://flit.readthedocs.io/en/latest/>`_ and `poetry
<https://python-poetry.org/>`_. Unfortunately, neither supports
reading the version number from git: flit reads it from the package's
``__version__`` variable (`flit bug 257
<https://github.com/takluyver/flit/issues/257>`_) and poetry hardcodes
it in the ``pyproject.toml`` file, neither of which seem like the
right solution as it duplicates information from the source of truth:
git. So I'm still using setuptools, but I should probably consider
`moving the metadata to setup.cfg
<https://setuptools.readthedocs.io/en/latest/setuptools.html#configuring-setup-using-setup-cfg-files>`_
for the static ones (like trove classifiers) that do not need to be
present at runtime.
