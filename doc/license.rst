License
=======

Unless otherwise noted, the content here is distributed under an
`Expat license <https://directory.fsf.org/wiki/License:Expat>`_ since
code snippets are small and we want to encourage code
reuse.

.. include:: ../LICENSE
